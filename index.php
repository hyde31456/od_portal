<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
	<!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="description" content="開放資料文字雲">
		<meta name="viewport" content="width=device-width,initial-scale=1">

		<title>OD Portal</title>

		<!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

		<!-- Bootstrap core CSS -->
		<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="css/jquerysctipttop.css" rel="stylesheet" type="text/css">
		
		<!-- Custom CSS -->
		<link href="css/wordcloud.css" rel="stylesheet" type="text/css">

		<!-- Custom styles for this template -->
		<link href="css/agency.min.css" rel="stylesheet" type="text/css">
    <link href="css/custom.css" rel="stylesheet" type="text/css">

	</head>

	<body>

	<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="/wordcloud">Logo</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <!-- Menu -->
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <!-- <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#team">Team</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
            </li> -->
          </ul>
        </div>
      </div>
	</nav>
	
	<!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-heading text-uppercase">The Ultimate Open Data Portal</div>
          
          <form id="searchbar" name="sentMessage" novalidate="novalidate">
            <div class="row">
              <div class="col-md-10 offset-md-1 col-10 offset-1">
                  <div class="form-group input-group mb-3">
                      <input class="form-control" id="inputKeyword" type="text" placeholder="Ex: 環境" required="required" data-validation-required-message="請輸入欲查詢的關鍵字" aria-label="Keywords">
                      <p class="help-block text-danger"></p>
                      <div class="input-group-append">
                        <button id="btnCustom" class="btn" type="button">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                      </div>
                    </div>
                <!-- <div class="form-group">
                  
                </div> -->
              </div>
            </div>
          </form>
          <!-- <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">Tell Me More</a> -->

        </div>
      </div>
    </header>

		<?php 

		/* check active */
		$current_city = 'all';
		$filter = '';

		if(isset($_GET["country"])) 
		{
			$current_city = $_GET["country"];
			$filter .= '<a href="' .$_SERVER['PHP_SELF']. '">全部</a>';
		}
		else
		{
			$filter .= '<a class="active" href="' .$_SERVER['PHP_SELF']. '">全部</a>';
		}

		$fp = fopen('csv/taiwan.csv', 'r');
		while (($data = fgetcsv($fp, 1000, ',')) !== FALSE) {

				$weight = intval(Sqrt($data[6]) * 2);
				$country = $data[1];

				if (intval($data[0]) < 10)
				{
					$countrystr = "0".$data[0];
				}
				else
				{
					$countrystr = $data[0];
				}

				$country_link = $countrystr."_".str_replace(" ","_",strtolower($data[2]));

				if ($country_link === $current_city) 
				{
					$filter .= '<span data-weight="'.$weight.'"><a class="active" href="?country=' .$country_link. '">'.$country.'</a></span>';    					
				}
				else
				{
					$filter .= '<span data-weight="'.$weight.'"><a href="?country=' .$country_link. '">'.$country.'</a></span>';    					
				} 
				// $filter .= '<a href="?country='.$countrystr."_".str_replace(" ","_",strtolower($data[2])).'"><span data-weight="'.$weight.'">'.$country.'</span></a>';    					
		}   

		if($current_city === 'all') {
		
			// 全部 factor = 1 or 2

			$all_dir = scandir('./csv_out');
			$arr = array();

			$cloud = '<div id="wordcloud" class="wordcloud">';

			foreach($all_dir as $dir_) {

				if ($dir_ !== '..' && $dir_ !== '.')
				{
					$fp = fopen('csv_out/' . $dir_ , 'r');
					$no = 0;
			
					while (($data = fgetcsv($fp, 1000, ",")) !== FALSE) {      
					
						if(count($data)>10 && $no!=0) 
						{
								$rr = explode(' ', $data[count($data) - 1]);
								
								foreach ($rr as $value) 
								{
									if(strlen($value) > 3)
									{
										if(array_key_exists($value, $arr))
										{
											$arr[$value] = $arr[$value] + 1;
										}
										else
										{
											$arr[$value] = 1;
										}
									}
								}            
						}
						$no++;
					} // end of while
				} // end of if else
			} // end of foreach
	
			foreach (array_keys($arr) as $value)
			{
				// 去除地名於文字雲中
				// 待改用 regular expression
				switch($value) {
					case '臺北市':
					case '新北市':
					case '桃園市':
					case '新竹市':
					case '台南市':
					case '宜蘭縣':
					case '新竹縣':
					case '台中市':
					case '高雄市':
					case '金門市':
					case '南投縣':
					case '嘉義市':
					case '澎湖縣':
					case '台東縣':
					case '屏東縣':
					case '基隆市':
					case '苗栗縣':
					case '彰化縣':
					case '雲林縣':
					case '嘉義縣':
					case '花蓮縣':
					case '連江縣':
						break;
					default: 
						if($arr[$value] > 1)
						{
							$link = 'layer3.php?q='.$value;
							$cloud .= '<span class="d-none" data-weight="'.intval(Sqrt($arr[$value]) * 10).'"><a href="'.$link.'">'.$value.'</a></span>';
						}
						break;
				} // end of switch
			}
		}
		else
		{
				// 縣市 factor = 3

				$city = $current_city;
				$cloud = '<div id="wordcloud" class="wordcloud">';
				$file = './csv_out/' .$city. '.csv';
				$arr = getCityData($file);
				
				foreach (array_keys($arr) as $value) {
					if($arr[$value] > 1)
					{
						$weight = intval(Sqrt($arr[$value]) * 10);
						$link 	= 'layer3.php?country=' .$city. '&q=' .$value;
						$cloud .= '<span class="d-none" data-weight="'.$weight.'"><a href="'.$link.'">'.$value.'</a></span>';
					}
				}  
	
	}

	function getCityData($file) {

		$city_array = array();
		$number 	= 0;
		
		$fp = fopen($file, 'r');

		while (($data = fgetcsv($fp, 1000, ',')) !== FALSE) {      
		
			if(count($data) > 10 && $number !== 0) 
			{
				$rr = explode(' ', $data[count($data) - 1]);
					
				foreach ($rr as $value) {

					if(strlen($value) > 3)
					{
						if(array_key_exists($value, $city_array))
						{
							$city_array[$value] = $city_array[$value] + 1;
						}
						else
						{
							$city_array[$value] = 1;
						}
					}
				}            
			}
			$number++;
		} // end of while

		return $city_array;
	}
	
	/* sort array */
	function ArrSort($Array) {
		$tag = array();
		$num = array();
		foreach($Array as $key => $value) {
			$tag[] = $key;
			$num[] = $value;
		}
		array_multisort($num, SORT_DESC, $tag, SORT_ASC, $Array);
		return $Array;
	}
	
	?>

		<!-- Cloud Grid -->
		<section id="cloud">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="tags">
							<?php echo $filter; ?>
						</div><!-- // tags -->
					</div>				 	
					<div class="col-12"> 
						<?php echo $cloud; ?>
					</div><!-- // wordcloud -->
				</div>
			</div>
		</section>
		
		<!-- Services -->
			<section id="services">
				<div class="container ">
					<div class="row text-center">
						<div class="col-md-3 col-sm-6 col-12">
							<span class="fa-stack fa-4x">
								<i class="fa fa-circle fa-stack-2x text-primary"></i>
								<i class="fa fa-pie-chart fa-stack-1x fa-inverse features"></i>
						</span>
							
							<h4 class="service-heading">Feature 1</h4>
							<!-- <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p> -->
						</div>
						<div class="col-md-3 col-sm-6 col-12">
							<span class="fa-stack fa-4x">
								<i class="fa fa-circle fa-stack-2x text-primary"></i>
								<i class="fa fa-line-chart fa-stack-1x fa-inverse"></i>
							</span>
							<h4 class="service-heading">Feature 2</h4>
							<!-- <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p> -->
						</div>
						<div class="col-md-3 col-sm-6 col-12">
								<span class="fa-stack fa-4x">
										<i class="fa fa-circle fa-stack-2x text-primary"></i>
										<i class="fa fa-bar-chart fa-stack-1x fa-inverse"></i>
									</span>
							<h4 class="service-heading">Feature 3</h4>
							<!-- <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p> -->
						</div>
						<div class="col-md-3 col-sm-6 col-12">
							<span class="fa-stack fa-4x">
								<i class="fa fa-circle fa-stack-2x text-primary"></i>
								<i class="fa fa-area-chart fa-stack-1x fa-inverse"></i>
							</span>
							<h4 class="service-heading">Feature 4</h4>
							<!-- <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p> -->
						</div>
					</div>
				</div>
		</section>
		
		<!-- Footer -->
		<footer>
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<span class="copyright">Open Data Portal 2018</span>
						</div>
						<div class="col-md-4">
							<!-- <ul class="list-inline social-buttons">
								<li class="list-inline-item">
									<a href="#">
										<i class="fa fa-twitter"></i>
									</a>
								</li>
								<li class="list-inline-item">
									<a href="#">
										<i class="fa fa-facebook"></i>
									</a>
								</li>
								<li class="list-inline-item">
									<a href="#">
										<i class="fa fa-linkedin"></i>
									</a>
								</li>
							</ul> -->
						</div>
						<div class="col-md-4">
							<!-- <ul class="list-inline quicklinks">
								<li class="list-inline-item">
									<a href="#">Privacy Policy</a>
								</li>
								<li class="list-inline-item">
									<a href="#">Terms of Use</a>
								</li>
							</ul> -->
						</div>
					</div>
				</div>
		</footer>

		<script src="js/jquery-3.3.1.js"></script>
		<!-- Bootstrap core JavaScript -->
		<script src="js/jquery.awesomeCloud-0.2.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

		<!--[if lt IE 7 ]>
		<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
		<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->
		<!-- Custom scripts for this template -->
		<script src="js/script.js"></script>
		<script src="js/agency.js"></script>
        
	</body>
</html>
