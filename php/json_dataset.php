<?php


	$func = $_REQUEST["func"];

    switch ($func) {
       
        case "getChartData":
            $echo = getChartData();
            break;
    }

    echo json_encode($echo);


    // 取得製作圖表資料
    function getChartData() {

        $callback = array();

        try{

                $country = $_REQUEST['country'];             // 縣市
                $current_dataset_title = $_REQUEST['c_d_t']; // 當前瀏覽的資料集名稱
                $usage = $_REQUEST['usage'];
                
                $fp = fopen("../csv_out/".$country.".csv", "r");
                
                $no 		 = 0;
                $count 		 = 0;
                $arr 		 = array();
                $arr_values  = array();

                $current_dataset = array();
                
                // 尋找資料集資料, 相似資料集
                while (($data = fgetcsv($fp, 10000, ",")) !== FALSE)
                {  
                    $arr_values[$data[1]] = similar_text($data[1], $current_dataset_title);
                    $no = $no + 1;
                    if($current_dataset_title === $data[1]) {
                        $current_dataset = $data;
                    }
                }
                
                fclose($fp);

                // 找不到該資料集時
                if(count($current_dataset) < 1) {
                    $callback['data'] = 'dataset not found';
                    $callback['success'] = false;
                }

                $arr_values3 = ArrSort($arr_values);
                $arr_values3 = array_slice($arr_values3, 1, 5);

                // 收集其他資料集的資料
                $arr_length = count($arr_values3);
                $other_dataset = array();

                $fp = fopen("../csv_out/".$country.".csv", "r");
                
                // 尋找資料集資料, 相似資料集
                while (($data = fgetcsv($fp, 10000, ",")) !== FALSE)
                {  
                    foreach (array_keys($arr_values3) as $value)
                    {
                        if($value === $data[1]) {
                            array_push($other_dataset, $data);
                            $arr_length--;
                        }
                        
                        if($arr_length < 0) {
                            break;
                        }
                    }
                }

                
                if($arr_length > 0) {
                    $callback['data'] = 'no recommed dataset';
                    $callback['success'] = false;
                }

                fclose($fp);

                // 整理格式
                $result = array();
                $result['name'] = array();
                
                $result['data']     = array(); // 資料量
                $result['download'] = array(); // 下載次數
                $result['view']     = array(); // 瀏覽次數
                
                /* [6]: 資料量, [7]: 下載次數, [8]: 瀏覽次數 */
                switch($usage) {

                    case 'datalength':
                        foreach ($other_dataset as $value)
                        {
                            $name = $value[1];
                            // $abbr = mb_substr($name, 0, 3, "UTF-8").'...';
                            $val          = intval($value[6]);
                            $download_val = intval($value[7]);
                            $view_val     = intval($value[8]);
                            
                            // $ann = $value[1];
                            array_push($result['name'], $name);
                            array_push($result['data'], $val);
                            array_push($result['download'], $download_val);
                            array_push($result['view'], $view_val);
                        }
                        break;

                }
                
                // var_dump($result);
                //  var chart = [
                // 		['City', '2010 Population',],
                // 		['New York City, NY', 8175000],
                // 		['Los Angeles, CA', 3792000],
                // 		['Chicago, IL', 2695000],
                // 		['Houston, TX', 2099000],
                // 		['Philadelphia, PA', 1526000]
                // 	]

                $callback['data'] = $result;
                $callback['success'] = true;


        } catch (Exception $e)

        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }

        return $callback;
    }

    function ArrSort($Array) {
        
        $tag = array();
        $num = array();

        foreach($Array as $key => $value) 
        {
            $tag[] = $key;
            $num[] = $value;
        }

        array_multisort($num, SORT_DESC, $tag, SORT_ASC, $Array);
        return $Array;
    }

?>