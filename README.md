# OD-Protal 開放資料平台

開放資料平台雛形

## Getting Started

詳細如以下說明使用

### Prerequisites

軟體需求安裝

```
apache server
php7
```

### Installing

直接將檔案放在伺服器網站目錄底下執行即可

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
