$(document).ready(function() {
    var factor = 2;
    if(getParameterByName('country') !== '') 
    {
        factor = 3;
    }
    resize(factor);
    // $(window).on("resize", function() {   
    //     resize(factor);      				
    // });
});

function resize(factor)
{    
    $('#wordcloud').outerHeight(786);
    
    // $('#wordcloud').outerHeight($(window).height()-$('#wordcloud').offset().top- Math.abs($('#wordcloud').outerHeight(true) - $('#wordcloud').outerHeight()));
    console.log($('#awesomeCloudwordcloud').css('height'));
    // 
    // $('#wordcloud').css('height', $('#awesomeCloudwordcloud').css('height'));

    makeCloud(factor);
}

function makeCloud(factor)
{
    $('#awesomeCloudwordcloud').remove();

    $("#wordcloud").awesomeCloud({
        "size" : {
            "grid" : 3,
            "factor" : factor
        },
        "color" : {
            "background" : "rgba(255,255,255,0)"
        },
        "options" : {
            "color" : "random-dark",
            "rotationRatio" : 0,
            "printMultiplier" : 2
        },
        "font" : "'微軟正黑體', 'Open San', Times, serif",
        "shape" : "square"
    });
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}