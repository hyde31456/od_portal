<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta name="description" content="開放資料文字雲">
<meta name="viewport" content="width=device-width,initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<script src="jquery.awesomeCloud-0.2.js"></script>
<style type="text/css">
.wordcloud {
height: 786px;
margin: 0.5in auto;
padding: 0;
page-break-after: always;
page-break-inside: avoid;
width: 1024px;
}
.wordcloud2 {
height: 786px;
margin: 0.5in auto;
padding: 0;
page-break-after: always;
page-break-inside: avoid;
width: 1024px;
}
</style>
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
</head>
<body>
<?php //echo $_GET["name"]; ?>


<div role="main">

<?php 
if(!isset($_GET["country"])){
$fp = fopen("taiwan.csv", "r");
echo "<fieldset><legend>縣市</legend>".'<a href="?">'.'全部'.'&nbsp;&nbsp;</a>';
   while (($data = fgetcsv($fp, 1000, ",")) !== FALSE) {
    if (intval($data[0])<10){
      $countrystr = "0".$data[0];
    }
    else{
      $countrystr = $data[0];
    }
    echo '<span data-weight="'.intval(Sqrt($data[6])*2).'"><a href="?country='.$countrystr."_".str_replace(" ","_",strtolower($data[2])).'">'.$data[1].'&nbsp;&nbsp;</a></span>';    
  }    


  //echo "<span><a>&nbsp;&nbsp;已知局處筆數/查詢全部資料：".$no_gm_count."/".$count."</a></span>"."</fieldset>";
echo "</fieldset>"; 
echo '<div id="wordcloud" class="wordcloud"> ';
$all_dir = scandir('./csv_out');
$arr=array();
foreach ($all_dir as $dir_) {
  if ($dir_!='..' and $dir_!='.'){
  $fp = fopen("./csv_out/".$dir_, "r");
  $no=0;
 
  while (($data = fgetcsv($fp,1000, ",")) !== FALSE) {      
    #echo '<span data-weight="'."1".'"><a href="?name='.$data[11].'">'.$data[11].'</a></span>';
    if(count($data)>10 && $no!=0){
        #echo $data[11]."   ".$no."<br>";
        $rr = explode(" ", $data[count($data)-1]);
        foreach ($rr as $value) {
          if (strlen($value)>3){
            if (array_key_exists($value,$arr)){
              $arr[$value] = $arr[$value]+1;
            }
            else{
            $arr[$value] =1;
          }
        }
    }            

      }
    $no++;
  }
}
}

foreach (array_keys($arr) as $value) {
    if($arr[$value]>1){
    //echo '<span data-weight="'.intval(Sqrt($arr[$value])*10).'"><a href="?q='.$value.'">'.$value.'</a></span>';
      echo '<span data-weight="'.intval(Sqrt($arr[$value])*10).'">'.$value.'</span>';
  
  }
}
}

else{
      $fp = fopen("taiwan.csv", "r");
    echo "<fieldset><legend>縣市</legend>".'<a href="?">'.'全部'.'&nbsp;&nbsp;</a>';
      while (($data = fgetcsv($fp, 1000, ",")) !== FALSE) {
        if (intval($data[0])<10){
          $countrystr = "0".$data[0];
        }
        else{
          $countrystr = $data[0];
        }
        echo '<span data-weight="'.intval(Sqrt($data[6])*2).'"><a href="?country='.$countrystr."_".str_replace(" ","_",strtolower($data[2])).'">'.$data[1].'&nbsp;&nbsp;</a></span>';    
      }    


      //echo "<span><a>&nbsp;&nbsp;已知局處筆數/查詢全部資料：".$no_gm_count."/".$count."</a></span>"."</fieldset>";
    echo "</fieldset>"; 
      echo '<div id="wordcloud" class="wordcloud2"> ';
      $fp = fopen("./csv_out/".$_GET["country"].".csv", "r");
      $no=0;
      $arr=array();
      while (($data = fgetcsv($fp,1000, ",")) !== FALSE) {  
        
        #echo '<span data-weight="'."1".'"><a href="?name='.$data[11].'">'.$data[11].'</a></span>';
        if(count($data)>10 && $no!=0){
            #echo $data[11]."   ".$no."<br>";
            $rr = explode(" ", $data[count($data)-1]);
            foreach ($rr as $value) {
              if (strlen($value)>3){
                if (array_key_exists($value,$arr)){
                  $arr[$value] = $arr[$value]+1;
                }
                else{
                $arr[$value] =1;
              }
            }
        }             

          }
        $no++;
      }
      foreach (array_keys($arr) as $value) {
        if($arr[$value]>1){
        echo '<span data-weight="'.intval(Sqrt($arr[$value])*10).'"><a href="index.php?q='.$value.'&country='.$_GET["country"].'">'.$value.'</a></span>';
      }
      }  

}
?>



<script>
			$(document).ready(function(){
				$("#wordcloud").awesomeCloud({
					"size" : {
						"grid" : 3,
						"factor" : 3
					},
					"color" : {
						"background" : "#ffffff"
					},
					"options" : {
						"color" : "random-dark",
						"rotationRatio" : 0.5,
						"printMultiplier" : 3
					},
					"font" : "'標楷體', Times, serif",
					"shape" : "square"
				});
			});
		</script> 
<!--[if lt IE 7 ]>
		<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
		<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->
        

</body>
</html>
