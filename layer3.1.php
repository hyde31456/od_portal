<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
	<!--<![endif]-->
	<head>
	 	<meta charset="utf-8">
		<meta name="description" content="開放資料文字雲">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<title>OD Portal</title>

		<!-- Custom fonts for this template -->
    	<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

		<!-- Bootstrap core CSS -->
		<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="css/jquerysctipttop.css" rel="stylesheet" type="text/css">
		
		<!-- Custom CSS -->
		<link href="css/wordcloud.css" rel="stylesheet" type="text/css">

		<!-- Custom styles for this template -->
		<link href="css/agency.min.css" rel="stylesheet" type="text/css">
    	<link href="css/custom.css" rel="stylesheet" type="text/css">
	</head>

	<body>

		<!-- Navigation -->
		<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
				<div class="container">
					<a class="navbar-brand js-scroll-trigger" href="/wordcloud">Logo</a>
					<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
						<!-- Menu -->
						<i class="fa fa-bars"></i>
					</button>
					<div class="collapse navbar-collapse" id="navbarResponsive">
						<ul class="navbar-nav text-uppercase ml-auto">
							<!-- <li class="nav-item">
								<a class="nav-link js-scroll-trigger" href="#services">Services</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger" href="#about">About</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger" href="#team">Team</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
							</li> -->
						</ul>
					</div>
				</div>
		</nav>
	
		<!-- Header -->
		<header class="masthead">
			<div class="container">
			<div class="intro-text">
			<div class="intro-heading text-uppercase">The Ultimate Open Data Portal</div>
				<form id="searchbar" name="sentMessage" novalidate="novalidate">
					<div class="row">
						<div class="col-md-10 offset-md-1 col-10 offset-1">
							<div class="form-group input-group mb-3">
								<input class="form-control" id="inputKeyword" type="text" placeholder="Ex: 環境" required="required" data-validation-required-message="請輸入欲查詢的關鍵字" aria-label="Keywords">
								<p class="help-block text-danger"></p>
								<div class="input-group-append">
								<button id="btnCustom" class="btn" type="button">
									<i class="fa fa-search" aria-hidden="true"></i>
								</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			</div>
		</header>

		<?php 

					// 簡易分頁
					function pagination($total, $content, $current, $q, $country, $param, $paramValue) {

						$callback = '';
						$limit 	  = 15; 					// 每一頁 15 項
						$pages 	  = floor($total/$limit);	// 總頁數

						if(is_array($paramValue)) {
							$paramValue = 'coldp';
						}
						
						$link = 'layer3.php?q='.$q.'&country='.$country.'&'.$param.'='.$paramValue;
				
						$page_html = '<ul class="pagination">';

						switch(true) {
							 
							case $total <= $limit:
								$page_html .= '<li class="page-item disabled"><a href="#" class="page-link" aria-label="Previous"><span aria-hidden="true">?</span><span class="sr-only">Previous</span></a></li>'
											.'<li class="page-item active disabled"><a href="#" class="page-link">1</a></li>'
											.'<li class="page-item disabled"><a href="#" class="page-link" aria-label="Next"><span aria-hidden="true">?</span><span class="sr-only">Next</span></a></li>';
								break;

							case $total > $limit:

								$prev = ($current-1);
								$next = ($current+1);

								if($prev != 0) {
									$page_html .= '<li class="page-item"><a href="' .$link. '&page=' .$prev. '" class="page-link" aria-label="Previous"><span aria-hidden="true">?</span><span class="sr-only">Previous</span></a></li>';	
								} else {
									$page_html .= '<li class="page-item disabled"><a href="#" class="page-link" aria-label="Previous"><span aria-hidden="true">?</span><span class="sr-only">Previous</span></a></li>';
								}

								$start 	= ($current-2) > 0 ? ($current-2) : 1;
								$end 	= ($current+2) < $pages ? ($current+2) : ($pages+1);

								if($start === 1 && $end < $pages) {
									$end = 5;
								}
								if($end === ($pages+1)) {
									if(($start-2) > 0) {
										$start = $start - 2;
									}
									if(($start-1) > 0) {
										$start = $start - 1;
									}
								}

								for($i=$start; $i<=$end; $i++) {
									if($i === $current) {
										$page_html .= '<li class="page-item active"><a href="' .$link. '&page=' .$i. '" class="page-link">' .$i. '</a></li>';
									} else {
										$page_html .= '<li class="page-item"><a href="' .$link. '&page=' .$i. '" class="page-link">' .$i. '</a></li>';
									}
								}

								if($next <= $pages) {
									$page_html .= '<li class="page-item"><a href="' .$link. '&page=' .$next. '" class="page-link" aria-label="Next"><span aria-hidden="true">?</span><span class="sr-only">Next</span></a></li>';
								} else {
									$page_html .= '<li class="page-item disabled"><a href="#" class="page-link" aria-label="Next"><span aria-hidden="true">?</span><span class="sr-only">Next</span></a></li>';
								}
						
								break;

							default:
								break;

						}

						$callback = $page_html;

						return $callback;
					}

					// 篩選列表
					function getDataset($q, $country, $filter, $given, $start, $end) {

						$callback = array();
						$fp = fopen("./csv_out/".$country.".csv", "r");
						
						$no 		 = 0;
						$count 		 = 0;
						$no_gm_count = 0;

						$arr 	   = array();
						$arr_value = array();
						$arr_room  = array();
						
						$list = '';

						switch($filter) {

							case 'department':

								while (($data = fgetcsv($fp,10000, ",")) !== false) {  
									
									if(count($data) > 10 && $no != 0 
										&& strpos($data[1], $q)  !== false 
										&& strpos($data[2], $given) !== false)
									{
										$count = $count + 1;	
										$link = 'dataset.php?q='.$q.'&country='.$country.'&dp='.$given.'&name='.$data[1];
										$title = $data[1];
										$arr[$data[1]] =  $data[1];
										// $list_maincolumn = $data[9];
	
										if($count >= $start && $count < $end) {
											$list .= '<tr class="d-flex" data-status="pagado">'.
												'<td class="col-12">'.
													'<div class="media" data-href="' .$link. '">'.
													'<div class="media-body">'.
													'<h4 class="title">' .$title. '</h4>'.
													// '<p class="summary">-</p>'.
														'</div>'.
														'</div>'.
													'</td>'.
												'</tr>';
										}
									}
									$no = $no + 1;
								}

								fclose($fp);

								break;

							case 'column':

								while (($data = fgetcsv($fp,10000, ",")) !== FALSE) {  
									
									if(count($data)>10 && $no!=0 && strpos($data[1], $q)  !== false &&strpos($data[9], $given)  !== false){
									
										$count= $count+1;	
										$link = 'dataset.php?q='.$q.'&country='.$country.'&col='.$given.'&name='.$data[1];
										$title = $data[1];
										$arr[$data[1]] =  $data[1];

										if($count >= $start && $count < $end) {
											$list .= '<tr class="d-flex" data-status="pagado">'.
												'<td class="col-12">'.
													'<div class="media" data-href="' .$link. '">'.
													'<div class="media-body">'.
													'<h4 class="title">' .$title. '</h4>'.
													// '<p class="summary">-</p>'.
														'</div>'.
														'</div>'.
													'</td>'.
												'</tr>';
										}
									}
									$no = $no + 1;
								}

								fclose($fp);

								break;

							/* 當什麼都沒選擇的時候 */
							case 'all':

								fclose($fp);

								// var_dump($given['dp']);

								// department first, column second.
								foreach($given['dp'] as $value) 
								{
									$fp = fopen("./csv_out/".$country.".csv", "r");
								
									while (($data = fgetcsv($fp, 10000, ",")) !== false) {  
										
										if(count($data) > 10 && $no != 0 
											&& strpos($data[1], $q)  !== false 
											&& strpos($data[2], $value) !== false)
										{
											$count = $count + 1;	
											$link = 'dataset.php?q='.$q.'&country='.$country.'&dp='.$value.'&name='.$data[1];
											$title = $data[1];
											$arr[$data[1]] =  $data[1];
											// $list_maincolumn = $data[9];
		
											if($count >= $start && $count < $end) {
												$list .= '<tr class="d-flex" data-status="pagado">'.
													'<td class="col-12">'.
														'<div class="media" data-href="' .$link. '">'.
														'<div class="media-body">'.
														'<h4 class="title">' .$title. '</h4>'.
														// '<p class="summary">-</p>'.
															'</div>'.
															'</div>'.
														'</td>'.
													'</tr>';
											}
										}
										$no = $no + 1;
									}

									fclose($fp);
								}

								// department first, column second.
								foreach($given['col'] as $value) 
								{
									$fp = fopen("./csv_out/".$country.".csv", "r");
								
									while (($data = fgetcsv($fp, 10000, ",")) !== false) {  
										
										if(count($data) > 10 && $no != 0 
											&& strpos($data[1], $q)  !== false 
											&& strpos($data[9], $value) !== false)
										{
											$count = $count + 1;	
											$link = 'dataset.php?q='.$q.'&country='.$country.'&dp='.$value.'&name='.$data[1];
											$title = $data[1];
											$arr[$data[1]] =  $data[1];
											// $list_maincolumn = $data[9];
		
											if($count >= $start && $count < $end) {
												$list .= '<tr class="d-flex" data-status="pagado">'.
													'<td class="col-12">'.
														'<div class="media" data-href="' .$link. '">'.
														'<div class="media-body">'.
														'<h4 class="title">' .$title. '</h4>'.
														// '<p class="summary">-</p>'.
															'</div>'.
															'</div>'.
														'</td>'.
													'</tr>';
											}
										}
										$no = $no + 1;
									}

									fclose($fp);
								}
								
							$given = '全部';

							break;
						}

						$last = $count < ($end-1) ? $count : ($end-1);
						
						$callback['title']  = $given;
						$callback['count']  = $count;
						$callback['info']  	= '共' .$count. '筆，本頁顯示第' .$start. '-' .$last. '筆。';
						$callback['content'] = $list;

						return $callback;
					}

					function ArrSort($Array) {

						$tag = array();
						$num = array();

						foreach($Array as $key => $value){
							$tag[] = $key;
							$num[] = $value;
						}

						array_multisort($num, SORT_DESC, $tag, SORT_ASC, $Array);
						return $Array;
					}

					if(isset($_GET["q"]) && isset($_GET["country"])) {

						$country = $_GET["country"];
						$q 		 = $_GET["q"];
						$x 		 = 3;
						$p 		 = 1;

						// $bread_level1 = 'layer2.php?country=' . $country;
						// $bread_level2 = 'layer3.php?country=' . $country . '&q=' . $q;

						if(isset($_GET["page"]))
						{
							$p = $_GET["page"];
						}
					}
					else 
					{
						$x = 1;
					}

					switch ($x)
					{
						case 1:
							// echo '<div id="wordcloud" class="wordcloud"> ';
							// $fp = fopen("taiwan.csv", "r");
							// while (($data = fgetcsv($fp, 1000, ",")) !== FALSE) {
							// 	if (intval($data[0])<10){
							// 		$countrystr = "0".$data[0];
							// 	}
							// 	else{
							// 		$countrystr = $data[0];
							// 	}
							// 	echo '<span class="d-none" data-weight="'.intval(Sqrt($data[6])*2).'"><a href="layer.php?country='.$countrystr."_".str_replace(" ","_",strtolower($data[2])).'">'.$data[1].'</a></span>';    
							// }    
							break;

						# layer2
						case 2:

							// echo '<div id="wordcloud" class="wordcloud">';

							// $fp = fopen("./csv_out/".$country.".csv", "r");
							// $no = 0;
							// $arr = array();
							// while (($data = fgetcsv($fp,1000, ",")) !== FALSE) {
							// 	// echo '<span data-weight="'."1".'"><a href="?name='.$data[11].'">'.$data[11].'</a></span>';
							// 	if(count($data) > 10 && $no != 0) {
							// 		// echo $data[11]."   ".$no."<br>";
							// 		$rr = explode(" ", $data[count($data) - 1]);
							// 		foreach ($rr as $value) {
							// 			if (strlen($value) > 3) 
							// 			{
							// 				if (array_key_exists($value, $arr)) 
							// 				{
							// 					$arr[$value] = $arr[$value] + 1;
							// 				}
							// 				else
							// 				{
							// 					$arr[$value] = 1;
							// 				}
							// 			}
							// 		}  	  
							// 	}
							// 	$no++;
							// }
							
							// $cloud = ''; #文字雲
							// $list  = ''; #列表並未排序

							// $end_item   = intval($p) * 5 ; #結束
							// $start_item = intval($end_item - 4) ; #起始
							// $i = 1;

							// foreach (array_keys($arr) as $value) {
							// 	if($arr[$value] > 1)
							// 	{
							// 		$weight = intval(Sqrt($arr[$value])*10);
							// 		$cloud .= '<span class="d-none" data-weight="'.$weight.'"><a href="?q='.$value.'&country='.$country.'">'.$value.'</a></span>';
									
							// 		if($i >= $start_item && $i <= $end_item) 
							// 		{
							// 			$list .= '<tr class="d-flex" data-status="pagado">'.
							// 								'<td class="col-12">'.
							// 									'<div class="media">'.
							// 									'<div class="media-body">'.
							// 									'<h4 class="title">' .$value. '</h4>'.
							// 									'<p class="summary">' .$weight. '</p>'.
							// 									'</div>'.
							// 									'</div>'.
							// 								'</td>'.
							// 							'</tr>';
							// 		}
							// 		$i++;
							// 	}
							// }
							// echo $cloud;  
							
							break;

						# layer3
						case 3:

							$fp = fopen("./csv_out/".$country.".csv", "r");

							$no 			= 0;
							$count 			= 0;
							$no_gm_count 	= 0;

							$arr 		= array();
							$arr_value 	= array();
							$arr_room 	= array();

							while (($data = fgetcsv($fp,1000, ",")) !== FALSE) {  

								#echo '<span data-weight="'."1".'"><a href="?name='.$data[11].'">'.$data[11].'</a></span>';
								if(count($data)>10 && $no!=0 &&strpos($data[1], $q) !== false){
								
								$count = $count + 1;	
								//echo $data[11]."   ".$no."<br>";
								$arr[$data[1]] =  $data[1]; 	  	 	

								if(strpos($data[9], "、") != false){
									$value_col = explode("、", $data[9]);  	 
									foreach ($value_col as $value2) {
										if ($value2!='' ){
											//$arr_value[$value] = $value2;
											if (array_key_exists($value2,$arr_value)){

												$arr_value[$value2] = $arr_value[$value2]+1;
											}
											else{
												$arr_value[$value2] =1;
											}
										}
									} 				
								}
								else{

									$value_col = explode(" ", $data[9]);  	 
									foreach ($value_col as $value2) {
										if ($value2!=''){
											//$arr_value[$value] = $value2;
											if (array_key_exists($value2,$arr_value)){					
												$arr_value[$value2] = $arr_value[$value2]+1;
											}
											else{
												$arr_value[$value2] =1;
											}
											
										}
									}

								}

								if($data[2]!=''){
									$no_gm_count = $no_gm_count+1;
									$value_room = explode("、", $data[2]);   
										foreach ($value_room as $value3) {		
										
											if ($value3!=''){
											//$arr_value[$value] = $value2;
												if (array_key_exists($value3,$arr_room)){
													$arr_room[$value3] = $arr_room[$value3]+1;
												}
												else{
												$arr_room[$value3] =1;
												}
											}
										}
									} 
										
								}
								$no++;

							}

							/* 頁數 */
							$limit = 15; //每一頁要呈現幾筆	
							
							if(isset($_GET['page'])) {
								$current = intval($_GET['page']);
								$end  = ($current * $limit) + 1;
								$start = $end - $limit;
							} else {
								$current = 1;
								$start   = 1;
								$end  	 = $limit + 1;
							}
							
							$col_array 	= array();
							$dp_array 	= array();

							/* 右邊列表 */
							if(isset($_GET["dp"])) {

								$dp = $_GET["dp"];
								// $coldp_title = $dp;
								$result = getDataset($q, $country, 'department', $dp, $start, $end);
								$list_title 	= $result['title'];
								$list_count 	= $result['info'];
								$list_content 	= $result['content'];
								$list_total 	= $result['count'];
							
								$pagination = pagination($list_total, $list_content, $current, $q, $country, 'dp', $dp);

							} else if(isset($_GET['col'])) {
								
								$column = $_GET["col"];
								// $coldp_title = $column;
								$result = getDataset($q, $country, 'column', $column, $start, $end);
								$list_title 	= $result['title'];
								$list_count 	= $result['info'];
								$list_content 	= $result['content'];
								$list_total 	= $result['count'];
								
								$pagination = pagination($list_total, $list_content, $current, $q, $country, 'col' ,$column);
																
							} else {
								
								// $coldp_title = '無';
								$pagination = '';
								$list_title = '暫無結果';
								$list_count = '共 0 筆';
								$list_content 	= '-';
							}

							/* 左邊菜單 */

							$sidebar_html = '<div class="filter-content text-left"><div class="list-group list-group-flush">';
							$department_menu = $sidebar_html;
							$column_menu     = $sidebar_html;
							
							// $list_title = '已知局處筆數：' .$no_gm_count. ' / 查詢全部資料：'. $count;
							// $end_item   = intval($p) * 5 ; #結束
							// $start_item = intval($end_item - 4) ; #起始
							$i = 1;


							// echo "<fieldset><legend>"."已知局處筆數/查詢全部資料：".$no_gm_count."/".$count."</legend>";
							foreach (array_keys($arr_room) as $value3) {

								$value3 = htmlentities($value3);

								switch(true) {
									case $i < 6:
										$department_menu .= '<a href="layer3.php?q='.$q.'&country='.$country."&dp=".$value3.'" class="list-group-item d-flex" title="' .$value3. '"><span class="d-10">' .$value3. '</span><span class="float-right badge badge-light round d-2">(' .$arr_room[$value3]. ')</span></a>';
										break;
									case $i === 6:
										# 如無超過 5 筆就不會出現 more
										if($i < count(array_keys($arr_room)))
											$department_menu .= '<a href="#" class="list-group-item more" data-target="dp">更多 ?</a>';
										break;
									default:
										$department_menu .= '<a href="layer3.php?q='.$q.'&country='.$country."&dp=".$value3.'" class="list-group-item s-item d-none" title="' .$value3. '"><span class="d-10">' .$value3. '</span><span class="float-right badge badge-light round d-2">(' .$arr_room[$value3]. ')</span></a>';
										break;
								}

								array_push($dp_array, $value3);

								$i++;
							}

							$department_menu .= '<a href="#" class="list-group-item less d-none" data-target="dp">收回 ?</a></div></div>';

							# echo "<span><a>&nbsp;&nbsp;已知局處筆數/查詢全部資料：".$no_gm_count."/".$count."</a></span>"."</fieldset>";
							#$cloud = ''; #文字雲
							#$cloud .= '<div id="wordcloud" class="wordcloud"> ';

							$i = 1;

							foreach (array_keys($arr_value) as $value) {

								if($arr_value[$value] > 1)
								{
									/* 欄位篩選 */
									// $data_weight = intval(Sqrt($arr_value[$value]/2)*20);
									$data_weight = intval($arr_value[$value]);
									$value = htmlentities($value);
									
									if($i <= 30) {
										switch(true) {
											case $i < 6:
												$column_menu .= '<a href="layer3.php?q='.$q.'&country='.$country."&col=".$value.'" class="list-group-item d-flex" title="' .$value. '"><span class="d-10">' .$value. '</span><span class="float-right badge badge-light round d-2">(' .$data_weight. ')</span></a>';
												break;
											case $i === 6:
												if($i < count(array_keys($arr_value)))
													$column_menu .= '<a href="#" class="list-group-item more" data-target="column">更多 ?</a>';
												break;
											default:
												$column_menu .= '<a href="layer3.php?q='.$q.'&country='.$country."&col=".$value.'" class="list-group-item s-item d-none" title="' .$value. '"><span class="d-10">' .$value. '</span><span class="float-right badge badge-light round d-2">(' .$data_weight. ')</span></a>';
												break;
										}
										array_push($col_array, $value);
									}
									$i++;
									#$cloud .= '<span class="d-none" data-weight="'.intval(Sqrt($arr_value[$value]/2)*20).'"><a href="coldp.php?q='.$q.'&country='.$country."&col=".$value.'">'.$value.'</a></span>';
								}
							}  
							$column_menu .= '<a href="#" class="list-group-item less s-item d-none" data-target="column">收回 ?</a></div></div>';

							/* 如果沒有選擇，這邊處理 */
							if(!isset($_GET["dp"]) && !isset($_GET['col'])) {
								
								$all = array('dp'  => $dp_array,
											 'col' => $col_array);
								// $coldp_title = '全部';
								$result = getDataset($q, $country, 'all', $all, $start, $end);
								$list_title 	= $result['title'];
								$list_count 	= $result['info'];
								$list_content 	= $result['content'];
								$list_total 	= $result['count'];
								
								$pagination = pagination($list_total, $list_content, $current, $q, $country, 'all' ,$all);
							}
							
							break;

						case 4: 
							$fp = fopen("./csv_out/".$country.".csv", "r");
							$no=0;
							$count=0;
							$no_gm_count=0;
							$arr=array();
							$arr_value=array();
							$arr_room=array();
							while (($data = fgetcsv($fp,10000, ",")) !== FALSE) {  
								//echo $data[1]."   ".$no."<br>";
								#echo '<span data-weight="'."1".'"><a href="?name='.$data[11].'">'.$data[11].'</a></span>';
								if(count($data)>10 && $no!=0 &&strpos($data[1], $q)  !== false &&strpos($data[9], $col)  !== false){
								
								$count= $count+1;	
								echo '<a href="?q='.$q.'&country='.$country."&col=".$col."&name=".$data[1].'">'."<h3>".$data[1]."</h3></a>   主要欄位：".$data[9]."<br>";
								$arr[$data[1]] =  $data[1];

								
							}$no=$no+1;
							}break;

						case 5:  
							$fp = fopen("./csv_out/".$country.".csv", "r");
							$no=0;
							$count=0;
							$no_gm_count=0;
							$arr=array();
							$arr_values=array();
							$arr_room=array();
							while (($data = fgetcsv($fp,10000, ",")) !== FALSE) {  
								//echo $data[1]."   ".$no."<br>";
								#echo '<span data-weight="'."1".'"><a href="?name='.$data[11].'">'.$data[11].'</a></span>';
								//echo $data[1].":".similar_text($data[1],$name) ."<br>";
								$arr_values[$data[1]] = similar_text($data[1],$name);
								$no=$no+1;
							}
							$arr_values3 =ArrSort($arr_values);
							//$rank = asort($arr_values);
							//print_r($arr_value );
							$show=0;
							echo '<a href="https://www.google.com.tw/search?q='.$name.'">'."<h2>".$name."</h2></a><br><h4>相似資料集：</h4>";
							foreach (array_keys($arr_values3) as $value) {
								if ($show!=0 and $show<6){
								echo '<a href="https://www.google.com.tw/search?q='.$value.'">'.$value."</a><br>";
							}
							$show = $show+1;
							}
							break;
							
						case 6: 
							$fp = fopen("./csv_out/".$country.".csv", "r");
							$no=0;
							$count=0;
							$no_gm_count=0;
							$arr=array();
							$arr_value=array();
							$arr_room=array();
							while (($data = fgetcsv($fp,10000, ",")) !== FALSE) {  
								//echo $data[1]."   ".$no."<br>";
								#echo '<span data-weight="'."1".'"><a href="?name='.$data[11].'">'.$data[11].'</a></span>';
								if(count($data)>10 && $no!=0 &&strpos($data[1], $q)  !== false &&strpos($data[2], $dp)  !== false){
								
								$count= $count+1;	
								echo '<a href="?q='.$q.'&country='.$country."&name=".$data[1].'">'."<h3>".$data[1]."</h3></a>   主要欄位：".$data[9]."<br>";
								$arr[$data[1]] =  $data[1];

								
							}$no=$no+1;
							}break;
						default:
							echo "No data";
						}
					?>

		<!-- Cloud Grid -->
		<section class="" id="cloud">
			<div class="container">
				<div class="row">
					
					<!-- Category & Filters -->
					<div class="col-lg-3 col-md-4 col-12 text-center">
						<div class="card side-menu">
							<article class="card-group-item dp">
								<header class="card-header"><h6 class="side-title text-left">局處部門</h6></header>
								<?php echo $department_menu; ?>
							</article> <!-- card-group-item.// -->
							<article class="card-group-item column">
								<header class="card-header"><h6 class="side-title text-left">相關欄位</h6></header>
								<?php echo $column_menu; ?>
							</article><!-- card-group-item.// -->
						</div> 
					</div><!-- Category & Filters.// -->
						<div class="col-lg-9 col-md-8 col-12">
							<h3 class="side-title"><?php echo '篩選: '. $list_title; ?></h3>
							<p><?php echo $list_count; ?></p>
							<!-- Table -->
							<table class="table table-filter">
								<tbody>
									<?php echo $list_content; ?>
								</tbody>
							</table>
							<!-- Paginition -->
							<?php echo $pagination; ?>
							<!-- <ul class="pagination">
								<li class="page-item"><a href="#" class="page-link" aria-label="Previous"><span aria-hidden="true">?</span><span class="sr-only">Previous</span></a></li>
								<li class="page-item active"><a href="#" class="page-link">1</a></li>
								<li class="page-item"><a href="#" class="page-link">2</a></li>
								<li class="page-item"><a href="#" class="page-link">3</a></li>
								<li class="page-item"><a href="#" class="page-link">4</a></li>
								<li class="page-item"><a href="#" class="page-link">5</a></li>
								<li class="page-item"><a href="#" class="page-link" aria-label="Next"><span aria-hidden="true">?</span><span class="sr-only">Next</span></a></li>
							</ul> -->
					</div><!-- list -->
				</div><!-- row -->
			</div><!-- container -->
		</section>
	
		<!-- Services -->
		<section id="services">
			<div class="container ">
				<div class="row text-center">
				<div class="col-md-3 col-sm-6 col-12">
					<span class="fa-stack fa-4x">
					<i class="fa fa-circle fa-stack-2x text-primary"></i>
					<i class="fa fa-pie-chart fa-stack-1x fa-inverse features"></i>
				</span>
					
					<h4 class="service-heading">Feature 1</h4>
					<!-- <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p> -->
				</div>
				<div class="col-md-3 col-sm-6 col-12">
					<span class="fa-stack fa-4x">
					<i class="fa fa-circle fa-stack-2x text-primary"></i>
					<i class="fa fa-line-chart fa-stack-1x fa-inverse"></i>
					</span>
					<h4 class="service-heading">Feature 2</h4>
					<!-- <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p> -->
				</div>
				<div class="col-md-3 col-sm-6 col-12">
					<span class="fa-stack fa-4x">
						<i class="fa fa-circle fa-stack-2x text-primary"></i>
						<i class="fa fa-bar-chart fa-stack-1x fa-inverse"></i>
						</span>
					<h4 class="service-heading">Feature 3</h4>
					<!-- <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p> -->
				</div>
				<div class="col-md-3 col-sm-6 col-12">
					<span class="fa-stack fa-4x">
					<i class="fa fa-circle fa-stack-2x text-primary"></i>
					<i class="fa fa-area-chart fa-stack-1x fa-inverse"></i>
					</span>
					<h4 class="service-heading">Feature 4</h4>
					<!-- <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p> -->
				</div>
				</div>
			</div>
		</section>
		
		<!-- Footer -->
		<footer>
			<div class="container">
				<div class="row">
				<div class="col-md-4">
					<span class="copyright">Open Data Portal 2018</span>
				</div>
				<div class="col-md-4">
					<!-- <ul class="list-inline social-buttons">
					<li class="list-inline-item">
						<a href="#">
						<i class="fa fa-twitter"></i>
						</a>
					</li>
					<li class="list-inline-item">
						<a href="#">
						<i class="fa fa-facebook"></i>
						</a>
					</li>
					<li class="list-inline-item">
						<a href="#">
						<i class="fa fa-linkedin"></i>
						</a>
					</li>
					</ul> -->
				</div>
				<div class="col-md-4">
					<!-- <ul class="list-inline quicklinks">
					<li class="list-inline-item">
						<a href="#">Privacy Policy</a>
					</li>
					<li class="list-inline-item">
						<a href="#">Terms of Use</a>
					</li>
					</ul> -->
				</div>
				</div>
			</div>
		</footer>

		<script src="js/jquery-3.3.1.js"></script>
		<!-- Bootstrap core JavaScript -->
		<script src="js/jquery.awesomeCloud-0.2.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

		<script>
				$(document).ready(function() {
					
					// highline
					var dp 	   = getParameterByName('dp'),
						column = getParameterByName('col');

					if(dp != "") {

						$('.dp .list-group-item').each(function(index, value){
							var title = $(this).attr('title');
							if(dp === title) {
								$(this).addClass('highline');
								return;
							}
						});
						
					} else if (column != ""){

						$('.column .list-group-item').each(function(index, value){
							var title = $(this).attr('title');
							if(column === title) {
								$(this).addClass('highline');
								return;
							}
						});
						
					}
					
					// List click func
					$('.media').click(function() {
						var link = $(this).attr('data-href');
						location.href = link;
					});

					$('.more').click(function(e) {
						e.preventDefault();
						var target = $(this).attr('data-target');
						var items = '.' +target+ ' .s-item';
						$(this).fadeOut('slow', function() {
							$(items).removeClass('d-none');
							$(items).addClass('d-flex');
						});
					});

					$('.less').click(function(e) {
						e.preventDefault();
						var target = $(this).attr('data-target');
						var items  = '.' +target+ ' .s-item';
						var more   = '.' +target+ ' .more';
						$(items).removeClass('d-flex');
						$(items).addClass('d-none');
						$(more).fadeIn('slow');
						$('html, body').animate({ scrollTop: '327px' });
					});

				});

				function getParameterByName(name) {
					name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
					var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
						results = regex.exec(location.search);
					return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
				}
				
		</script> 
		<!--[if lt IE 7 ]>
		<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
		<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->

		<!-- Custom scripts for this template -->
		<script src="js/script.js"></script>
		<script src="js/agency.js"></script>
	</body>
</html>
