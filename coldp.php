<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
	<!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="description" content="開放資料文字雲">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<title>OD Portal</title>

		<!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

		<!-- Bootstrap core CSS -->
		<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
		
		<!-- Custom CSS -->
		<link href="css/wordcloud.css" rel="stylesheet" type="text/css">

		<!-- Custom styles for this template -->
		<link href="css/agency.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
	</head>

	<body>

		<!-- Navigation -->
		<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
				<div class="container">
					<a class="navbar-brand js-scroll-trigger" href="/wordcloud">Logo</a>
					<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
						<!-- Menu -->
						<i class="fa fa-bars"></i>
					</button>
					<div class="collapse navbar-collapse" id="navbarResponsive">
						<ul class="navbar-nav text-uppercase ml-auto">
							<!-- <li class="nav-item">
								<a class="nav-link js-scroll-trigger" href="#services">Services</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger" href="#about">About</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger" href="#team">Team</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
							</li> -->
						</ul>
					</div>
				</div>
		</nav>

		<?php 
					function ArrSort($Array){
							$tag = array();
							$num = array();

							foreach($Array as $key => $value){
								$tag[] = $key;
								$num[] = $value;
							}

							array_multisort($num, SORT_DESC, $tag, SORT_ASC, $Array);
							return $Array;
					}

					if(isset($_GET["q"])&&isset($_GET["country"])&&isset($_GET["dp"])){
						$country=$_GET["country"];
						$q = $_GET["q"];	
						$dp  = $_GET["dp"];
						
						$x = 6;

						$coldp_title = $dp;
						$bread_level1 = 'layer2.php?country='.$country;
						$bread_level2 = 'layer3.php?country='.$country.'&q='.$q;

					}
					else if(isset($_GET["q"])&&isset($_GET["country"])&&isset($_GET["name"])){
						$country=$_GET["country"];
						$q = $_GET["q"];
						
						$name  = $_GET["name"];
						$x=5;
					}
					else if(isset($_GET["q"])&&isset($_GET["country"])&&isset($_GET["col"])){
						$country=$_GET["country"];
						$q = $_GET["q"];
						$col = $_GET["col"];
						
						$x=4;

						$coldp_title = $col;
						$bread_level1 = 'layer2.php?country='.$country;
						$bread_level2 = 'layer3.php?country='.$country.'&q='.$q;
					}	
					else if(isset($_GET["q"])&&isset($_GET["country"])){
						$country=$_GET["country"];
						$q = $_GET["q"];
							$x=3;
						}
					else if(isset($_GET["country"])){
						$country=$_GET["country"];
							$x=2;
						}
					else{
							$x=1;
						}

					switch ($x)
					{
						case 1:
							// echo '<div id="wordcloud" class="wordcloud"> ';
							// $fp = fopen("taiwan.csv", "r");
							// while (($data = fgetcsv($fp, 1000, ",")) !== FALSE) {
							// 	if (intval($data[0])<10){
							// 		$countrystr = "0".$data[0];
							// 	}
							// 	else{
							// 		$countrystr = $data[0];
							// 	}
							// 	echo '<span class="d-none" data-weight="'.intval(Sqrt($data[6])*2).'"><a href="layer.php?country='.$countrystr."_".str_replace(" ","_",strtolower($data[2])).'">'.$data[1].'</a></span>';    
							// }    
							break;

						# layer
						case 2:

							// echo '<div id="wordcloud" class="wordcloud">';

							// $fp = fopen("./csv_out/".$country.".csv", "r");
							// $no = 0;
							// $arr = array();
							// while (($data = fgetcsv($fp,1000, ",")) !== FALSE) {
							// 	// echo '<span data-weight="'."1".'"><a href="?name='.$data[11].'">'.$data[11].'</a></span>';
							// 	if(count($data) > 10 && $no != 0) {
							// 		// echo $data[11]."   ".$no."<br>";
							// 		$rr = explode(" ", $data[count($data) - 1]);
							// 		foreach ($rr as $value) {
							// 			if (strlen($value) > 3) 
							// 			{
							// 				if (array_key_exists($value, $arr)) 
							// 				{
							// 					$arr[$value] = $arr[$value] + 1;
							// 				}
							// 				else
							// 				{
							// 					$arr[$value] = 1;
							// 				}
							// 			}
							// 		}  	  
							// 	}
							// 	$no++;
							// }
							
							// $cloud = ''; #文字雲
							// $list  = ''; #列表並未排序

							// $end_item   = intval($p) * 5 ; #結束
							// $start_item = intval($end_item - 4) ; #起始
							// $i = 1;

							// foreach (array_keys($arr) as $value) {
							// 	if($arr[$value] > 1)
							// 	{
							// 		$weight = intval(Sqrt($arr[$value])*10);
							// 		$cloud .= '<span class="d-none" data-weight="'.$weight.'"><a href="?q='.$value.'&country='.$country.'">'.$value.'</a></span>';
									
							// 		if($i >= $start_item && $i <= $end_item) 
							// 		{
							// 			$list .= '<tr class="d-flex" data-status="pagado">'.
							// 								'<td class="col-12">'.
							// 									'<div class="media">'.
							// 									'<div class="media-body">'.
							// 									'<h4 class="title">' .$value. '</h4>'.
							// 									'<p class="summary">' .$weight. '</p>'.
							// 									'</div>'.
							// 									'</div>'.
							// 								'</td>'.
							// 							'</tr>';
							// 		}
							// 		$i++;
							// 	}
							// }
							// echo $cloud;  
							
							break;

						# layer2
						case 3:  

							// $fp = fopen("./csv_out/".$country.".csv", "r");
							// $no=0;
							// $count=0;
							// $no_gm_count=0;
							// $arr=array();
							// $arr_value=array();
							// $arr_room=array();
							// while (($data = fgetcsv($fp,1000, ",")) !== FALSE) {  
								
							// 	#echo '<span data-weight="'."1".'"><a href="?name='.$data[11].'">'.$data[11].'</a></span>';
							// 	if(count($data)>10 && $no!=0 &&strpos($data[1], $q) !== false){
								
							// 	$count= $count+1;	
							// 	//echo $data[11]."   ".$no."<br>";
							// 	$arr[$data[1]] =  $data[1]; 	  	 	

							// 	if(strpos($data[9], "、") != false){
							// 		$value_col = explode("、", $data[9]);  	 
							// 		foreach ($value_col as $value2) {
							// 			if ($value2!='' ){
							// 				//$arr_value[$value] = $value2;
							// 				if (array_key_exists($value2,$arr_value)){

							// 					$arr_value[$value2] = $arr_value[$value2]+1;
							// 				}
							// 				else{
							// 					$arr_value[$value2] =1;
							// 				}
							// 			}
							// 		} 				
							// 	}
							// 	else{

							// 		$value_col = explode(" ", $data[9]);  	 
							// 		foreach ($value_col as $value2) {
							// 			if ($value2!=''){
							// 				//$arr_value[$value] = $value2;
							// 				if (array_key_exists($value2,$arr_value)){					
							// 					$arr_value[$value2] = $arr_value[$value2]+1;
							// 				}
							// 				else{
							// 					$arr_value[$value2] =1;
							// 				}
											
							// 			}
							// 		}

							// 	}

							// 	if($data[2]!=''){
							// 		$no_gm_count = $no_gm_count+1;
							// 		$value_room = explode("、", $data[2]);   
							// 			foreach ($value_room as $value3) {		
										
							// 				if ($value3!=''){
							// 				//$arr_value[$value] = $value2;
							// 					if (array_key_exists($value3,$arr_room)){
							// 						$arr_room[$value3] = $arr_room[$value3]+1;
							// 					}
							// 					else{
							// 					$arr_room[$value3] =1;
							// 					}
							// 				}
							// 			}
							// 		} 
										
							// 	}
							// 	$no++;

							// }

							// # $arr_value = array_unique($arr_value);

							// $list = ''; #右邊列表
							// $list_title = '已知局處筆數：' .$no_gm_count. ' / 查詢全部資料：'. $count;
							// $end_item   = intval($p) * 5 ; #結束
							// $start_item = intval($end_item - 4) ; #起始
							// $i = 1;

							// // echo "<fieldset><legend>"."已知局處筆數/查詢全部資料：".$no_gm_count."/".$count."</legend>";
							// foreach (array_keys($arr_room) as $value3) {

							// 	if($i >= $start_item && $i <= $end_item) 
							// 	{
							// 		$list .= '<tr class="d-flex" data-status="pagado">'.
							// 							'<td class="col-12">'.
							// 								'<div class="media" data-href=?q='.$q.'&country='.$country."&dp=".$value3.'" >'.
							// 								'<div class="media-body">'.
							// 								'<h4 class="title">' .$value3. '</h4>'.
							// 								'<p class="summary">(' .$arr_room[$value3]. ')</p>'.
							// 								'</div>'.
							// 								'</div>'.
							// 							'</td>'.
							// 						'</tr>';
							// 	}
							// 	$i++;
							// 	# $list .= '<span><a href="?q='.$q.'&country='.$country."&dp=".$value3.'">'.$value3.'('.$arr_room[$value3].') &nbsp;&nbsp;</a></span>';
							// }
							// # echo "<span><a>&nbsp;&nbsp;已知局處筆數/查詢全部資料：".$no_gm_count."/".$count."</a></span>"."</fieldset>";
							// // echo "</fieldset>";	
							// // echo "<fieldset><legend>"."主要欄位</legend>";

							// $cloud = ''; #文字雲
							// $cloud .= '<div id="wordcloud" class="wordcloud"> ';
							// foreach (array_keys($arr_value) as $value) {
							// 	if($arr_value[$value] > 1)
							// 	{
							// 		$cloud .= '<span class="d-none" data-weight="'.intval(Sqrt($arr_value[$value]/2)*20).'"><a href="?q='.$q.'&country='.$country."&col=".$value.'">'.$value.'</a></span>';
							// 	}
							// }  
							// // $cloud .= "</div>";
							
							// echo $cloud;

							// break;

						case 4: 
							$fp = fopen("./csv_out/".$country.".csv", "r");
							$no 					= 0;
							$count 				= 0;
							$no_gm_count 	= 0;

							$arr 					= array();
							$arr_value 		= array();
							$arr_room 		= array();
							
							$coldp_list = '';

							while (($data = fgetcsv($fp,10000, ",")) !== FALSE) {  
								//echo $data[1]."   ".$no."<br>";
								#echo '<span data-weight="'."1".'"><a href="?name='.$data[11].'">'.$data[11].'</a></span>';
								if(count($data)>10 && $no!=0 &&strpos($data[1], $q)  !== false &&strpos($data[9], $col)  !== false){
								
								$count= $count+1;	
								$list_title = '<a href="dataset.php?q='.$q.'&country='.$country."&col=".$col."&name=".$data[1].'">'."<h5>".$data[1]."</h5>";
								$list_maincolumn = $data[9];
								$arr[$data[1]] =  $data[1];
								
								$coldp_list .= '<tr class="d-flex">'.
																'<th class="col-4">' .$list_title. '</th>'.
																'<td class="col-8">' .$list_maincolumn. '</td>'.
															'</tr>';
								
							}$no=$no+1;
							}break;

						case 5:  

							// $fp = fopen("./csv_out/".$country.".csv", "r");

							// $no 				 = 0;
							// $count 			 = 0;
							// $no_gm_count = 0;

							// $arr 			  = array();
							// $arr_values = array();
							// $arr_room   = array();

							// while (($data = fgetcsv($fp, 10000, ",")) !== FALSE) {  
							// 	//echo $data[1]."   ".$no."<br>";
							// 	#echo '<span data-weight="'."1".'"><a href="?name='.$data[11].'">'.$data[11].'</a></span>';
							// 	//echo $data[1].":".similar_text($data[1],$name) ."<br>";
							// 	$arr_values[$data[1]] = similar_text($data[1], $name);
							// 	$no = $no + 1;
							// }
							// $arr_values3 = ArrSort($arr_values);
							// //$rank = asort($arr_values);
							// //print_r($arr_value );
							// $show = 0;
							// $dataset_title = $name;
							// $recommend_list = '';
							// $i = 1;
							// // echo '<a href="https://www.google.com.tw/search?q='.$name.'">'."<h2>".$name."</h2></a><br><h4>相似資料集：</h4>";
							// foreach (array_keys($arr_values3) as $value) {
							// 	if ($show != 0 && $show < 6)
							// 	{
							// 		$recommend_list .= '<tr class="d-flex" data-status="pagado">'.
							// 												'<td class="col-1 align-middle">'.$i.'.</td>'.
							// 												'<td class="col-11">'.
							// 													'<div class="media" data-href="https://www.google.com.tw/search?q='.$value.'">'.
							// 													'<div class="media-body">'.
							// 														'<h4 class="rec-title">'.
							// 															'<a href="https://www.google.com.tw/search?q='.$value.'">' .$value. '</a>'.
							// 														'</h4>'.
							// 													'</div>'.
							// 													'</div>'.
							// 													'</td>'.
							// 											 '</tr>';
							// 		#echo '<a href="https://www.google.com.tw/search?q='.$value.'">'.$value."</a><br>";
							// 		$i++;
							// 	}
							// 	$show = $show + 1;
							// }

							break;
							
						case 6: 

							$fp = fopen("./csv_out/".$country.".csv", "r");

							$no 				 = 0;
							$count 			 = 0;
							$no_gm_count = 0;

							$arr 				= array();
							$arr_value 	= array();
							$arr_room 	= array();

							$coldp_list = '';

							while (($data = fgetcsv($fp,10000, ",")) !== FALSE) {  
								//echo $data[1]."   ".$no."<br>";
								#echo '<span data-weight="'."1".'"><a href="?name='.$data[11].'">'.$data[11].'</a></span>';
								if(count($data) > 10 && $no != 0 && strpos($data[1], $q)  !== false && strpos($data[2], $dp)  !== false)
								{
								
									$count= $count+1;	
									$list_title = '<a href="dataset.php?q='.$q.'&country='.$country.'&dp='.$dp.'&name='.$data[1].'">'."<h5>".$data[1]."</h5></a>";
									$list_maincolumn = $data[9];
									$arr[$data[1]] =  $data[1];

									$coldp_list .= '<tr class="d-flex">'.
																	'<th class="col-4">' .$list_title. '</th>'.
																	'<td class="col-8">' .$list_maincolumn. '</td>'.
																'</tr>';
								}
								$no = $no + 1;
							}
							
							break;
						
						default:
							echo "No data";
						}
		?>


		<!-- Dataset Grid -->
    <section class="" id="cloud">
        <div class="container">
          <div class="block"></div>
          <div class="row">

						<nav class="col-12 breadcrumb">
							<a class="breadcrumb-item" href="<?php echo $bread_level1; ?>"><?php echo $country; ?></a>
							<a class="breadcrumb-item" href="<?php echo $bread_level2; ?>"><?php echo $q; ?></a>
							<span class="breadcrumb-item active"><?php echo $coldp_title; ?></span>
						</nav>

            <div class="col-lg-12 col-12 text-center">
              <h2 class="section-heading text-uppercase text-left"><?php echo $coldp_title; ?></h2>
              <!-- <p class="text-left">Description</p> -->
              <!-- Dataset Properties -->
              <!-- <div class="col-11"> -->
                <div class="dataset-property col-12 table-responsive">
                  <table class="table table-sm rtable">
											<thead>
												<tr class="d-flex">
													<th class="col-4">資料集</th>
													<th class="col-8">主要欄位</th>
												</tr>
											</thead>
                      <tbody>
													<?php echo $coldp_list; ?>
                          <!-- <tr class="d-flex">
														<th class="col-4">主要欄位</th>
                          	<td class="col-8"></td>
                          </tr> -->
                      </tbody>
                  </table>
                </div>
              <!-- </div> -->
            </div>
          </div>
        </div>
      </section>

		<!-- Services -->
    <section id="services">
      <div class="container ">
        <div class="row text-center">
          <div class="col-md-3 col-sm-6 col-12">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-pie-chart fa-stack-1x fa-inverse features"></i>
           </span>
            
            <h4 class="service-heading">Feature 1</h4>
            <!-- <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p> -->
          </div>
          <div class="col-md-3 col-sm-6 col-12">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-line-chart fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Feature 2</h4>
            <!-- <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p> -->
          </div>
          <div class="col-md-3 col-sm-6 col-12">
              <span class="fa-stack fa-4x">
                  <i class="fa fa-circle fa-stack-2x text-primary"></i>
                  <i class="fa fa-bar-chart fa-stack-1x fa-inverse"></i>
                </span>
            <h4 class="service-heading">Feature 3</h4>
            <!-- <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p> -->
          </div>
          <div class="col-md-3 col-sm-6 col-12">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-area-chart fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Feature 4</h4>
            <!-- <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p> -->
          </div>
        </div>
      </div>
	</section>
	
	<!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Open Data Portal 2018</span>
          </div>
          <div class="col-md-4">
            <!-- <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
            </ul> -->
          </div>
          <div class="col-md-4">
            <!-- <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
            </ul> -->
          </div>
        </div>
      </div>
    </footer>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
		<!-- Bootstrap core JavaScript -->
		<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script src="jquery.awesomeCloud-0.2.js"></script>
		
		<script>
				$(document).ready(function() {

				});
		</script> 
		<!--[if lt IE 7 ]>
		<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
		<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->

		<!-- Plugin JavaScript -->
		<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
		<!-- Custom scripts for this template -->
		<script src="js/script.js"></script>
		<script src="js/agency.js"></script>
	</body>
</html>
