<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
	<!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="description" content="開放資料文字雲">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<title>OD Portal</title>

		<!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

		<!-- Bootstrap core CSS -->
		<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/jquerysctipttop.css" rel="stylesheet" type="text/css">
		
		<!-- Custom CSS -->
		<link href="css/wordcloud.css" rel="stylesheet" type="text/css">

		<!-- Custom styles for this template -->
		<link href="css/agency.min.css" rel="stylesheet" type="text/css">
		<link href="css/custom.css" rel="stylesheet" type="text/css">
		
	</head>

	<body>

		<!-- Navigation -->
		<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
				<div class="container">
					<a class="navbar-brand js-scroll-trigger" href="/wordcloud">Logo</a>
					<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
						<!-- Menu -->
						<i class="fa fa-bars"></i>
					</button>
					<div class="collapse navbar-collapse" id="navbarResponsive">
						<ul class="navbar-nav text-uppercase ml-auto">
							<!-- <li class="nav-item">
								<a class="nav-link js-scroll-trigger" href="#services">Services</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger" href="#about">About</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger" href="#team">Team</a>
							</li>
							<li class="nav-item">
								<a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
							</li> -->
						</ul>
					</div>
				</div>
		</nav>
	
		<!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-heading text-uppercase">The Ultimate Open Data Portal</div>
					<form id="searchbar" name="sentMessage" novalidate="novalidate">
							<div class="row">
								<div class="col-md-10 offset-md-1 col-10 offset-1">
										<div class="form-group input-group mb-3">
												<input class="form-control" id="inputKeyword" type="text" placeholder="Ex: 環境" required="required" data-validation-required-message="請輸入欲查詢的關鍵字" aria-label="Keywords">
												<p class="help-block text-danger"></p>
												<div class="input-group-append">
													<button id="btnCustom" class="btn" type="button">
															<i class="fa fa-search" aria-hidden="true"></i>
													</button>
												</div>
											</div>
								</div>
							</div>
					</form>
        </div>
      </div>
    </header>

		<?php
                    
        include_once('php/global.php');

        // 1. querystr
        // 2. querystr & country
        // 3. querystr & country & department

        $filter = '';
        $bread = '';

        if (isset($_GET["q"]) && isset($_GET["country"]) && isset($_GET["dp"])) {
            $x = 4;
            $department = $_GET["dp"];
            $country    = $_GET["country"];
            $querystr   = $_GET["q"];
            $bread .= '<a class="breadcrumb-item" href="index.php?country=' .$country. '">' .$country. '</a>'
            .'<span class="breadcrumb-item active">' .$querystr. '</span>';

        } else if (isset($_GET["q"]) && isset($_GET["country"])) {
            $x = 3;
            $country  = $_GET["country"];
            $querystr = $_GET["q"];
            $bread .= '<a class="breadcrumb-item" href="index.php?country=' .$country. '">' .$country. '</a>'
                    .'<span class="breadcrumb-item active">' .$querystr. '</span>';

        } elseif (isset($_GET["q"])) {
            $x =    2;
            $querystr = $_GET["q"];

        } else {
            $x = 1;
        }
                    
        $cloud = '';
                    
        switch ($x) {
            case 1:

                $cloud .= '<h1>無相關資料。</h1>';
                break;

            case 2:

                // 無法查詢
                // $cloud .= '<div id="wordcloud" class="wordcloud">';

                // $fp = fopen('./csv_out/'.$country.'.csv', 'r');

                // $no 	= 0;
                // $arr 	= array();

                // while (($data = fgetcsv($fp, 1000, ",")) !== FALSE) {
                                
                // 	if(count($data) > 10 && $no != 0) {

                // 		$rr = explode(" ", $data[count($data) - 1]);

                //      foreach ($rr as $value)
                // 		{
                //          if (strlen($value) > 3)
                // 			{
                //              if (array_key_exists($value, $arr))
                // 				{
                // 					$arr[$value] = $arr[$value] + 1;
                // 				}
                // 				else
                // 				{
                // 					$arr[$value] = 1;
                // 				}
                // 			}
                //      }
                // 	}
                // 	$no++;
                // }
                            
                // $cloud = ''; #文字雲

                // foreach (array_keys($arr) as $value) {

                // 	if($arr[$value] > 1)
                // 	{
                // 		$weight = intval(Sqrt($arr[$value]) * 10);
                // 		$cloud .= '<span class="d-none" data-weight="'.$weight.'"><a href="layer3.php?q='.$value.'&country='.$country.'">'.$value.'</a></span>';
                // 	}
                // }
                // echo $cloud;
                            
                break;

            case 3:
                
                $factor = 2;

                $data = getCloudset($querystr, $country);
                $arr_value = $data['arr_value'];
                $arr_room = $data['arr_room'];

                // $arr_value = array_unique($arr_value);
                // arr_room, arr_value

                // 相關局處

                $filter .= '<h3>局處:</h3>';
                $filter .= '<span><a class="active" href="?q=' .$querystr. '&country=' .$country. '">無限制</a></span>';
                foreach (array_keys($arr_room) as $value3) {
                    $dp_count = $arr_room[$value3];
                    $dp_link = '?q=' .$querystr. '&country=' .$country. '&dp=' .$value3;
                    $filter .= '<span><a href="' .$dp_link. '">'.$value3.'('.$dp_count.')</a></span>';
                }
                // echo "<span><a>&nbsp;&nbsp;已知局處筆數/查詢全部資料：".$no_gm_count."/".$count."</a></span>"."</fieldset>";
								

                // 抓取「所有」局處底下的關鍵字並加總整合，加入至下方的文字雲中。
				$all_dp = array_keys($arr_room);
			    $all_dp_arr = array();
                foreach ($all_dp as $value) {
					array_push($all_dp_arr, getDataset($querystr, $country, $value));
				}

				// 處理所有局處底下主要欄位的出現次數。
				$columnArr = array();
				foreach ($all_dp_arr as $dp) {
					foreach ($dp as $value) {
						$colArr = $value['colArray'];
						foreach ($colArr as $column) {
						if ($column !== '' )
						{
							if (array_key_exists($column, $columnArr)) 
							{
							    $columnArr[$column] = $columnArr[$column] + (1 * $factor);
							}
							else
							{
                    			$columnArr[$column] = (1 * $factor);
                    		}
							}
                        }
                    }
                }
                                
				// 加入至主要欄位文字雲
				// $cloud_keyword = $arr_value;
				$cloud_keyword = array_merge($arr_value, $columnArr);
                $cloud .= '<div id="wordcloud" class="wordcloud">';
                foreach ($cloud_keyword as $key => $value) {
                    if ($value > 1) {
                        $cloud .= '<span class="d-none" data-weight="'.intval(Sqrt($value/2)*20).'"><a href="?q='.$querystr.'&country='.$country."&col=".$key.'">'.$key.'</a></span>';
                    }
                }
                $cloud .= "</div>";

                break;

            case 4:

                $factor = 3; // 倍率，為了讓特定單位的局處關鍵字可以顯示在文字雲中，以避免數量過少導致隱藏。
                
                $data = getCloudset($querystr, $country);
                $arr_value = $data['arr_value'];
                $arr_room = $data['arr_room'];

                // 相關局處
                $filter .= '<h3>局處:</h3>';
                $filter .= '<span><a href="?q=' .$querystr. '&country=' .$country. '">無限制</a></span>';
                foreach (array_keys($arr_room) as $value) {
                    $dp_count = $arr_room[$value];
                    $dp_link = '?q=' .$querystr. '&country=' .$country. '&dp=' .$value;
                    if($value === $department) {
                        $filter .= '<span><a class="active" href="' .$dp_link. '">'.$value.'('.$dp_count.')</a></span>';
                    } else {
                        $filter .= '<span><a href="' .$dp_link. '">'.$value.'('.$dp_count.')</a></span>';
                    }
                }

                // 抓取「特定」局處底下的關鍵字並加總整合，加入至下方的文字雲中。
			    $dp_arr = getDataset($querystr, $country, $department);
                
                // 處理所有局處底下主要欄位的出現次數。
				$columnArr = array();
				foreach ($dp_arr as $value) {
					$colArr = $value['colArray'];
					foreach ($colArr as $column) {
					if ($column !== '')
						{
                            // 倍率增加
							if (array_key_exists($column, $columnArr)) 
							{
							    $columnArr[$column] = $columnArr[$column] + (1 * $factor);
							}
							else
							{
                    			$columnArr[$column] = (1 * $factor);
                            }
						}
                    }
                }

                // 加入至主要欄位文字雲
				$cloud_keyword = array_merge($arr_value, $columnArr);
                $cloud .= '<div id="wordcloud" class="wordcloud">';
                foreach ($cloud_keyword as $key => $value) {
                    if ($value > 1) {
                        $cloud .= '<span class="d-none" data-weight="'.intval(Sqrt($value/2)*20).'"><a href="?q='.$querystr.'&country='.$country."&col=".$key.'">'.$key.'</a></span>';
                    }
                }
                $cloud .= "</div>";

                break;

            default:
                $cloud .= '<h1>無相關資料。</h1>';
        }

        function getDataset($querystr, $country, $department)
        {
                        
			$callback = array();
						
            $fp = fopen("./csv_out/".$country.".csv", "r");
                                                
            $no  = 0;
            $arr = array();
                   
            while (($data = fgetcsv($fp, 10000, ",")) !== false) {
								if (count($data) > 10 
										&& $no != 0
										&& strpos($data[1], $querystr)  !== false
										&& strpos($data[2], $department) !== false) {

											$title = $data[1];
											$arr[$data[1]] = $data[1];
                    	$link = 'dataset.php?q='.$querystr.'&country='.$country.'&dp='.$department.'&name='.$title;
											$column = $data[9];

											if(strpos($column, "、") !== false)
												$column_arr = explode("、", $column);
											else
												$column_arr = explode(" ", $column);
											
											array_push($callback, array('title'=>$title, 
																									'link'=> $link, 
																									'column'=>$column,
																									'colArray'=>$column_arr));
                }
              	$no = $no + 1;
						}
                        
            fclose($fp);
            
            return $callback;
        }

        function getCloudset($querystr, $country) 
        {

            $callback = array();

            $fp = fopen('./csv_out/'.$country.'.csv', 'r');
            
            $no    = 0;
            $count = 0;
            $no_gm_count = 0;

            $arr = array();
            $arr_value = array();
            $arr_room = array();

            while (($data = fgetcsv($fp, 1000, ",")) !== false) {
                // echo '<span data-weight="'."1".'"><a href="?name='.$data[11].'">'.$data[11].'</a></span>';
                            
                if (count($data)>10 && $no != 0 && strpos($data[1], $querystr) !== false) {
                    $count++;

                    //echo $data[11]."   ".$no."<br>";
                    $arr[$data[1]] = $data[1];

                    if (strpos($data[9], "、") != false) {
                        $value_col = explode("、", $data[9]);
                                    
                        foreach ($value_col as $value2) {
                            if ($value2 != '') {
                                //$arr_value[$value] = $value2;
                                if (array_key_exists($value2, $arr_value)) {
                                    $arr_value[$value2] = $arr_value[$value2] + 1;
                                } else {
                                    $arr_value[$value2] = 1;
                                }
                            }
                        }
                    } else {
                        $value_col = explode(' ', $data[9]);
                        foreach ($value_col as $value2) {
                            if ($value2 != '') {
                                //$arr_value[$value] = $value2;
                                if (array_key_exists($value2, $arr_value)) {
                                    $arr_value[$value2] = $arr_value[$value2] + 1;
                                } else {
                                    $arr_value[$value2] = 1;
                                }
                            }
                        }
                    }

                    if ($data[2] != '') {
                        $no_gm_count++;
                        $value_room = explode("、", $data[2]);
                                    
                        foreach ($value_room as $value3) {
                            if ($value3 != '') {
                                //$arr_value[$value] = $value2;
                                if (array_key_exists($value3, $arr_room)) {
                                    $arr_room[$value3] = $arr_room[$value3] + 1;
                                } else {
                                    $arr_room[$value3] = 1;
                                }
                            }
                        }
                    }
                }
                $no++;
            }

            $callback['arr_value'] = $arr_value;
            $callback['arr_room'] = $arr_room;

            fclose($fp);

            return $callback;
        }

        ?>

        <!-- Cloud Grid -->
        <section class="" id="cloud">
            <div class="container">
                <div class="row">
                    <nav class="col-12 breadcrumb">
                        <?php echo $bread; ?>
                    </nav><!-- // bread -->
                    <div class="col-12 col-md-8">
                        <div class="tags">
                            <?php echo $filter; ?>
                        </div><!-- // tags -->
                    </div>
                    <div class="col-12 col-md-4 view-btn">
                        
                        <div class="btn-group btn-group-toggle">
                            <label class="btn btn-primary active">
                                <input type="radio" name="options" id="option1" autocomplete="off" checked> 文字雲
                            </label>
                            <label class="btn btn-primary">
                                <input type="radio" name="options" id="option1" autocomplete="off"> 列 表
                            </label>
                        </div><!-- // view type -->
                    </div>  
                    <div class="col-12">
                        <?php echo $cloud; ?>
                    </div><!-- // cloud -->
            </div>
                
            </div><!-- row -->
    </div><!-- container -->
    </section>
    
    <!-- Services -->
    <section id="services">
      <div class="container ">
        <div class="row text-center">
          <div class="col-md-3 col-sm-6 col-12">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-pie-chart fa-stack-1x fa-inverse features"></i>
           </span>
            
            <h4 class="service-heading">Feature 1</h4>
            <!-- <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p> -->
          </div>
          <div class="col-md-3 col-sm-6 col-12">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-line-chart fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Feature 2</h4>
            <!-- <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p> -->
          </div>
          <div class="col-md-3 col-sm-6 col-12">
              <span class="fa-stack fa-4x">
                  <i class="fa fa-circle fa-stack-2x text-primary"></i>
                  <i class="fa fa-bar-chart fa-stack-1x fa-inverse"></i>
                </span>
            <h4 class="service-heading">Feature 3</h4>
            <!-- <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p> -->
          </div>
          <div class="col-md-3 col-sm-6 col-12">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-area-chart fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Feature 4</h4>
            <!-- <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p> -->
          </div>
        </div>
      </div>
    </section>
    
    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Open Data Portal 2018</span>
          </div>
          <div class="col-md-4">
            <!-- <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
            </ul> -->
          </div>
          <div class="col-md-4">
            <!-- <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
            </ul> -->
          </div>
        </div>
      </div>
    </footer>

        <script src="js/jquery-3.3.1.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script src="js/jquery.awesomeCloud-0.2.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!--[if lt IE 7 ]>
        <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
        <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
        <![endif]-->

        <!-- Custom scripts for this template -->
        <script src="js/script.js"></script>
        <script src="js/agency.js"></script>
    </body>
</html>
